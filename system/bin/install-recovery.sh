#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:30408014:306586b2c6774736b81e1af209ad3e8801c50b4b; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:26948938:01f84d891da34af6703ec116a7811f5a0c066df6 EMMC:/dev/block/bootdevice/by-name/recovery 306586b2c6774736b81e1af209ad3e8801c50b4b 30408014 01f84d891da34af6703ec116a7811f5a0c066df6:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
